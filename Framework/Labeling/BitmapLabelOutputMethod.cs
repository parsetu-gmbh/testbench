﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Parsetu.Labeling
{
    public class BitmapLabelOutputMethod : ILabelOutputMethod
    {
        private readonly int _width;
        private readonly int _height;

        public BitmapLabelOutputMethod (int width, int height)
        {
            _width = width;
            _height = height;
        }

        public ILabelOutputResult Output(ILabel label)
        {
            var bitmap = new Bitmap(_width, _height);

            Graphics g = Graphics.FromImage(bitmap);
            label.Create(g);
            return new BitmapLabelOutputResult(true) { Bitmap = bitmap };
        }
    }
}
