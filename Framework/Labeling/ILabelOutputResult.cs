﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsetu.Labeling
{
    public interface ILabelOutputResult
    {
        bool Success { get; }
    }
}
