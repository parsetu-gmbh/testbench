﻿using System.Drawing;

namespace Parsetu.Labeling
{

    public class BitmapLabelOutputResult : ILabelOutputResult
    {
        public BitmapLabelOutputResult(bool result)
        {
            Success = result;
        }

        public Bitmap Bitmap { get; set; }

        public bool Success { get; }
    }
}