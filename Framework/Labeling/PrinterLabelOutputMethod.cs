﻿using System.Drawing.Printing;

namespace Parsetu.Labeling
{
    public class PrinterLabelOutputMethod : ILabelOutputMethod
    {
        private readonly string _printerName;
        private ILabel _label;

        public PrinterLabelOutputMethod(string printerName)
        {
            _printerName = printerName;
        }

        public ILabelOutputResult Output(ILabel label)
        {
            _label = label;

            PrintDocument printDocument = new PrintDocument { PrinterSettings = { PrinterName = _printerName } };

            if(printDocument.PrinterSettings.IsValid)
            {
                printDocument.PrintPage += new PrintPageEventHandler(PrintDoc_PrintPage);
                printDocument.Print();
                printDocument.PrintPage -= PrintDoc_PrintPage;
                return new PrinterLabelOutputResult(true);
            }

            return new PrinterLabelOutputResult(false);
        }

        private void PrintDoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            _label.Create(e.Graphics);
        }
    }
}