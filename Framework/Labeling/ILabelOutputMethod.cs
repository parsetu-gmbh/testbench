﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parsetu.Labeling
{
    public interface ILabelOutputMethod
    {
        ILabelOutputResult Output(ILabel label);
    }
}
