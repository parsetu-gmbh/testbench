﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using log4net;
using Serilog;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Parsetu.Logging
{
    public class Logger<T> : ILogger
    {
        private readonly ILog _logger;

        public Serilog.ILogger _seriLogger { get; private set; }

        public static ILogger Create()
        {
            var a = new Logger<T>();
            a._seriLogger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.Log4Net().CreateLogger().ForContext(typeof(T));
            return a;
        }

        public void Debug(string format, params object[] args)
        {
            _logger?.DebugFormat(format, args);
            _seriLogger.Debug(format, args);
        }

        public void Info(string format, params object[] args)
        {
            _logger?.InfoFormat(format, args);
            _seriLogger.Information(format, args);
        }

        public void Warning(string format, params object[] args)
        {
            _logger?.WarnFormat(format, args);
            _seriLogger.Warning(format, args);
        }

        public void Error(string format, params object[] args)
        {
            _logger?.ErrorFormat(format, args);
            _seriLogger.Error(format, args);
        }

        public void Fatal(string format, params object[] args)
        {
            _logger?.FatalFormat(format, args);
            _seriLogger.Fatal(format, args);
        }

        public void Debug(Exception exp, string format, params object[] args)
        {
            _logger?.Debug(string.Format(format, args), exp);
            _seriLogger.Debug(exp, format, args);
        }

        public void Info(Exception exp, string format, params object[] args)
        {
            _logger?.Info(string.Format(format, args), exp);
            _seriLogger.Information(exp, format, args);
        }

        public void Warning(Exception exp, string format, params object[] args)
        {
            _logger?.Warn(string.Format(format, args), exp);
            _seriLogger.Warning(exp, format, args);
        }

        public void Error(Exception exp, string format, params object[] args)
        {
            _logger?.Error(string.Format(format, args), exp);
            _seriLogger.Error(exp, format, args);
        }

        public void Fatal(Exception exp, string format, params object[] args)
        {
            _logger?.Fatal(string.Format(format, args), exp);
            _seriLogger.Fatal(exp, format, args);
        }
    }
}
