﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using Parsetu.Logging.Appender;

namespace Parsetu.Logging
{
    public static class LogConfiguration
    {
        public static void Configure()
        {
            XmlConfigurator.Configure();
        }

        public static void Configure(string configFile)
        {
            XmlConfigurator.Configure(new FileInfo(configFile));
        }

        public static void Configure(FileInfo configFile)
        {
            XmlConfigurator.Configure(configFile);
        }

        public static IUIAppender GetUIAppende()
        {
            var appenders = LogManager.GetRepository().GetAppenders();
            return appenders.OfType<IUIAppender>().FirstOrDefault();
        }
    }
}
