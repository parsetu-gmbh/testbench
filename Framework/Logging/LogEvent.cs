﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parsetu.Logging.Appender;

namespace Parsetu.Logging
{
    public class LogEvent
    {
        public DateTime TimeStamp { get; set; }

        public string Logger { get; set; }

        public Level Level { get; set; }

        public string Message { get; set; }
    }
}
