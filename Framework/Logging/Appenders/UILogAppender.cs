﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;
using Parsetu.Logging;

namespace Parsetu.Logging.Appender
{
    public interface IUIAppender
    {
        void Subscribe(Action<LogEvent> logEvent);
    }

    public enum Level
    {
        Info,
        Warn,
        Error,
    }
    public class UILogAppender : AppenderSkeleton, IUIAppender
    {
        private Action<LogEvent> _messageReceived;

        protected override void Append(LoggingEvent loggingEvent)
        {
            if(loggingEvent.Level >= Threshold)
            {
                var logEvent = new LogEvent
                {
                    TimeStamp = loggingEvent.TimeStamp,
                    Logger = loggingEvent.LoggerName,
                    Message = loggingEvent.RenderedMessage,
                    Level = TransformLevel(loggingEvent.Level),
                };
                _messageReceived?.Invoke(logEvent);
            }
        }

        private Level TransformLevel(log4net.Core.Level level)
        {
            if(level.Value <= 50000)
            {
                return Level.Info;
            }
            if (level.Value <= 60000)
            {
                return Level.Warn;
            }
            return Level.Error;
        }

        public void Subscribe(Action<LogEvent> logEvent)
        {
            _messageReceived = logEvent;
        }
    }
}
