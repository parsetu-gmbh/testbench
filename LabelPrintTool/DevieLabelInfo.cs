﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Printing;
using Parsetu.Labeling;

namespace LabelPrintTool
{
    public class DeviceLabelInfo
    {
        public string ArticalNumber { get; set; }
        public string DeviceType { get; set; }
        public string SerialNumber { get; set; }
        public string Pin1Name { get; set; }

        public string Pin2Name { get; set; }

        public string Pin3Name { get; set; }

        public string Pin4Name { get; set; }

        public string Pin5Name { get; set; }
        public string ProductionDate { get; set; }

    }

    public class DeviceLabel : ILabel 
    {
        private readonly DeviceLabelInfo _labelInformation;
        public DeviceLabel(DeviceLabelInfo deviceLabelInfo)
        {
            _labelInformation = deviceLabelInfo;
        }

        public void Create(Graphics e)
        {
            Font fontArial10 = new Font("Arial", 10, FontStyle.Regular);
            Font fontArial8 = new Font("Arial", 8, FontStyle.Regular);
            Font fontArial8B = new Font("Arial", 8, FontStyle.Bold);

            Image imagePomtava = Properties.Resources.Pomtava;
            e.DrawImage(imagePomtava, 3, 3, 70, 80);

            Image imageCE = Properties.Resources.ce_mark;
            e.DrawImage(imageCE, 100, 3, 30, 30);

            Image imageSocket = Properties.Resources.Stecker_5Polig;
            e.DrawImage(imageSocket, 75, 3, 25, 25);

            e.DrawString("Artical Nr.:", fontArial8B, Brushes.Black, 2, 80);
            e.DrawString(_labelInformation.ArticalNumber, fontArial8, Brushes.Black, 60, 80);

            e.DrawString("TYPE:", fontArial8B, Brushes.Black, 2, 90);
            e.DrawString(_labelInformation.DeviceType, fontArial8, Brushes.Black, 40, 90);

            e.DrawString("S/N:", fontArial8B, Brushes.Black, 2, 100);
            e.DrawString(_labelInformation.SerialNumber, fontArial8, Brushes.Black, 25, 100);

            e.DrawString("MADE IN GERMANY", fontArial8, Brushes.Black, 2, 110);
            e.DrawString(_labelInformation.ProductionDate, fontArial8, Brushes.Black, 130, 110);

        }
    }
}
