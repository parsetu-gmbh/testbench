﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Parsetu.Labeling;
using LabelPrintTool.Properties;

namespace LabelPrintTool
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private bool _pomtavaLogoSelected;
        private bool _ciponLogoSlected;
        private bool _noLogo = true;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PrintLabel_Click(object sender, RoutedEventArgs e)
        {
            DeviceLabelInfo labelInfo = new DeviceLabelInfo
            {
                ArticalNumber = ArticalNumber.Text,
                DeviceType = DeviceType.Text,
                SerialNumber = SerialNumber.Text,
                Pin1Name = "GND",
                ProductionDate = ProductionDate.Text,
            };
            ILabel dLebel = new DeviceLabel(labelInfo);
            
            PrintLabel(dLebel);
        }

        private void PrintPostLabel_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PrintLabel(ILabel label)
        { 
            PrinterLabelOutputMethod printer = new PrinterLabelOutputMethod(Settings.Default.PrinterName);
            printer.Output(label);
        }

        public bool PomtavaIsSelected 
        { get { return _pomtavaLogoSelected; }
            set 
            {
                if (value == _pomtavaLogoSelected) return;
                _pomtavaLogoSelected = value;
            } 
        }

        public bool CiponIsSelected
        {
            get { return _ciponLogoSlected; }
            set
            {
                if (value == _ciponLogoSlected) return;
                _ciponLogoSlected = value;
            }
        }

        public bool NoLogoIsSelected
        {
            get { return _noLogo; }
            set
            {
                if (value == _noLogo) return;
                _noLogo = value;
            }
        }
        public string Pin1 { get; set; }
        public string Pin2 { get; set; }
        public string Pin3 { get; set; }
        public string Pin4 { get; set; }
        public string Pin5 { get; set; }

    }
}
