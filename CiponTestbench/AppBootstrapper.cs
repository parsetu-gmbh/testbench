﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using CiponTestbench.UI.ViewModel;
using Parsetu.Logging;

namespace CiponTestbench
{
    public class AppBootstrapper : BootstrapperBase
    {
        private SimpleContainer _container;

        private static ILogger Logger = Logger<AppBootstrapper>.Create();

        public AppBootstrapper()
        {
            var config = new TypeMappingConfiguration
            {
                DefaultSubNamespaceForViewModels = "ViewModel",
                DefaultSubNamespaceForViews = "View"
            };
            ViewLocator.ConfigureTypeMappings(config);

            LogConfiguration.Configure();

            Initialize();
        }

        protected override void Configure()
        {
            base.Configure();

            _container = new SimpleContainer();
            _container.Singleton<IWindowManager, WindowManager>();
            _container.Singleton<IEventAggregator, EventAggregator>();
            _container.PerRequest<IShellViewModel, ShellViewModel>();
        }

        protected override void OnUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Error(e.Exception.Message);
            Logger.Info(e.Exception.StackTrace);
            if(e.Exception.InnerException != null)
            {
                Logger.Error(e.Exception.InnerException.Message);
                Logger.Info(e.Exception.InnerException.StackTrace);
            }
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<IShellViewModel>();
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return _container.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }

}