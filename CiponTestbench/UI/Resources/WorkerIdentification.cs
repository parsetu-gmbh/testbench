﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Markup;
using System.Xml;

namespace CiponTestbench.UI.Resources
{
    public class Worker
    {

        public int Number { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }

        public string Initials { get; set; }

        public string FullName
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }
    }

    public class WorkerList : List<Worker>
    {

    }

    public static class WorkerLoader
    {
        public static WorkerList Load(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            FileStream fileStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read);
            return (WorkerList)XamlReader.Load(fileStream);
        }

        public static void Save(string fileName, WorkerList workers)
        {
            XmlWriter xmlWriter = XmlWriter.Create(fileName, new XmlWriterSettings
            {
                Indent = true,
                NewLineOnAttributes = true
            });
            XamlWriter.Save(workers, xmlWriter);
        }
    }
}
