﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace CiponTestbench.UI.Resources
{
    public static class PasswordBoxAssistant
    {
        public static readonly DependencyProperty BoundPassword =
            DependencyProperty.RegisterAttached("BoundPassword", typeof(string), typeof(PasswordBoxAssistant), new FrameworkPropertyMetadata(string.Empty, OnBoundPasswordChanged));

        public static readonly DependencyProperty BindPassword =
            DependencyProperty.RegisterAttached("BindPassword", typeof(bool), typeof(PasswordBoxAssistant), new FrameworkPropertyMetadata(false, OnBindPasswordChanged));

        private static readonly DependencyProperty UpdatingPassword =
            DependencyProperty.RegisterAttached("UpdatingPassword", typeof(bool), typeof(PasswordBoxAssistant), new PropertyMetadata(false));

        private static void OnBoundPasswordChanged(DependencyObject dO, DependencyPropertyChangedEventArgs arg)
        {
            PasswordBox box = dO as PasswordBox;

            if(dO == null || !GetBindPassword(dO))
            {
                return;
            }

            box.PasswordChanged -= HandlePasswordChanged;

            string newPassword = (string)arg.NewValue;

            if(!GetUpdatingPassword(box))
            {
                box.Password = newPassword;
            }

            box.PasswordChanged += HandlePasswordChanged;
        }

        private static void OnBindPasswordChanged(DependencyObject dO, DependencyPropertyChangedEventArgs eArg)
        {
            PasswordBox box = dO as PasswordBox;
            
            if(box == null)
            {
                return;
            }

            bool wasBound = (bool)(eArg.OldValue);
            bool needToBind = (bool)(eArg.NewValue);

            if(wasBound)
            {
                box.PasswordChanged -= HandlePasswordChanged;
            }

            if (needToBind)
            {
                box.PasswordChanged += HandlePasswordChanged;
            }
        }

        private static void HandlePasswordChanged(object sender, RoutedEventArgs eArgs)
        {
            PasswordBox box = sender as PasswordBox;

            SetUpdatingPassword(box, true);

            SetBoundPassword(box, box.Password);
            SetUpdatingPassword(box, false);
        }

        public static void SetBindPassword(DependencyObject dO, bool value)
        {
            dO.SetValue(BindPassword, value);
        }

        public static bool GetBindPassword(DependencyObject dO)
        {
            return (bool)dO.GetValue(BindPassword);
        }

        public static void SetBoundPassword(DependencyObject dO, string value)
        {
            dO.SetValue(BoundPassword, value);
        }

        public static string GetBoundPassword(DependencyObject dO)
        {
            return (string)dO.GetValue(BoundPassword);
        }

        public static void SetUpdatingPassword(DependencyObject dO, bool value)
        {
            dO.SetValue(UpdatingPassword, value);
        }

        public static bool GetUpdatingPassword(DependencyObject dO)
        {
            return (bool)dO.GetValue(UpdatingPassword);
        }
    }
}
