﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;
using System.Diagnostics;
using Parsetu.Logging;
using CiponTestbench.UI.Resources;


namespace CiponTestbench.UI.ViewModel
{
    public class ShellViewModel : Screen, IShellViewModel
    {
        private static ILogger Logger = Logger<ShellViewModel>.Create();

        public ShellViewModel()
        {
            Logger.Info("ShellViewModel Created");

            DisplayName = "Cipon EOL Testbench";
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        protected override void OnViewAttached(object view, object context)
        {
            base.OnViewAttached(view, context);

            UserLoginViewModel vm = new UserLoginViewModel(WorkerLoader.Load("Worker.xml"));
            IoC.Get<IWindowManager>().ShowDialog(vm);
            if(vm.Result)
            {

            }
            else
            {
                TryClose();
            }
            
        }
    }
}