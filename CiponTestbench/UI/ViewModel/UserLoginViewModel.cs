﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Caliburn.Micro;
using CiponTestbench.UI.Resources;
using Parsetu.Logging;


namespace CiponTestbench.UI.ViewModel
{
    public class UserLoginViewModel: Screen, IDataErrorInfo
    {
        private static ILogger Logger = Logger<UserLoginViewModel>.Create();

        private readonly WorkerList _userList;
        
        public UserLoginViewModel(WorkerList userList)
        {
            _userList = userList;
            NotifyOfPropertyChange(() => InputValid);
        }

        public List<Worker> Users
        {
            get { return _userList; }
        }


        public bool InputValid 
        { 
            get
            {
                return UserExists()
                    && PasswordCorrect();
            }
        }

        private bool UserExists()
        {
            Worker user = _userList.FirstOrDefault(w => w.FullName == UserName);
            return user != null;
        }

        private bool PasswordCorrect()
        {
            Worker user = _userList.FirstOrDefault(w => w.FullName == UserName);
            if(user != null)
            {
                if(user.Password == Password)
                {
                    return true;
                }
            }
            return false;
        }

        public string this[string columnName]
        {
            get
            {
                NotifyOfPropertyChange(() => InputValid);
                switch (columnName)
                {
                    case "Password":
                        {
                            if (PasswordCorrect() != true)
                            {
                                return "Ungültige Kennwort";
                            }

                            break;
                        }
                    //case "UserName":
                    //    {
                    //        if (UserExists() != true)
                    //        {
                    //            return "Ungültige Benutzername";
                    //        }
                    //        break;
                    //    }
                }
                return string.Empty;
            }
        }
        public string Error { get; private set; }
        public string UserName { get; set; }

        public string Password { get; set; }
        public bool Result { get; set; }

        public void Confirm()
        {
            Result = true;
            TryClose();
        }

        public void Close()
        {
            TryClose();
        }

    }
}